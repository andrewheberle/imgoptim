# imgoptim

This is an Alpine Linux based image that includes the following tools:
* optipng
* svgo
* jpegoptim

## Usage

The intended use case for this image is for CI purposes to optimise images
during the build process of a website.

### Optimise PNG images

```sh
docker run -v images:/images registry.gitlab.com/andrewheberle/imgoptim:latest find /images -type f -regex '.*\.\(png\)$' -exec optipng -o 6 -i 0 -strip all {} \;
```

### Optimise JPEG images

```sh
docker run -v images:/images registry.gitlab.com/andrewheberle/imgoptim:latest find /images -type f -regex '.*\.\(jpg\|jpeg\)$' -exec jpegoptim --strip-all --all-progressive {} \;
```

### Optimise SVG images

```sh
docker run -v images:/images registry.gitlab.com/andrewheberle/imgoptim:latest find /images -type f -regex '.*\.\(svg\)$' -exec svgo {} \;
```
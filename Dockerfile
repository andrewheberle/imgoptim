FROM alpine:latest

ADD rootfs /

ENV WEBP_QUALITY=80 \
    INPUT_DIR=/images \
    OUTPUT_DIR=

RUN chmod +x /usr/local/bin/*.sh && \
    apk --update-cache add --virtual .run-deps nodejs && \
    apk add --virtual .build-deps npm && \
    apk add libwebp-tools && \
    apk add jpegoptim && \
    apk add optipng && \
    npm install -g svgo && \
    apk del .build-deps && \
    rm -rf /var/cache/apk/*

#!/bin/sh
if [ "${OUTPUT_DIR}" != "" ]; then
    find "${INPUT_DIR}" -type f -regex '.*\.\(jpg\|jpeg\)$' -exec /bin/sh -c 'cwebp -q ${WEBP_QUALITY} {} -o "${OUTPUT_DIR}/$(basename "{}").webp"' \;
else
    find "${INPUT_DIR}" -type f -regex '.*\.\(jpg\|jpeg\)$' -exec cwebp -q ${WEBP_QUALITY} {} -o "{}.webp" \;
fi
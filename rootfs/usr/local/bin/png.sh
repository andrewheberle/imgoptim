#!/bin/sh
find "${INPUT_DIR}" -type f -regex '.*\.\(png\)$' -exec optipng -o 6 -i 0 -strip all -quiet {} \;
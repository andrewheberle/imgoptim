#!/bin/sh
find "${INPUT_DIR}" -type f -regex '.*\.\(jpg\|jpeg\)$' -exec jpegoptim --strip-all --all-progressive {} \;